<?php
/**
 * Test for PMA_GIS_Linestring
 *
 * @package phpMyAdmin-test
 */

require_once 'PMA_GIS_Geometry_test.php';
require_once 'libraries/gis/pma_gis_geometry.php';
require_once 'libraries/gis/pma_gis_linestring.php';

/**
 * Tests for PMA_GIS_Linestring class
 */
class PMA_GIS_LinestringTest extends PMA_GIS_GeometryTest
{
    /**
     * @var    PMA_GIS_Linestring
     * @access protected
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     * @return nothing
     */
    protected function setUp()
    {
        $this->object = PMA_GIS_Linestring::singleton();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     *
     * @access protected
     * @return nothing
     */
    protected function tearDown()
    {
        unset($this->object);
    }

    /**
     * data provider for testGenerateWkt
     *
     * @return data for testGenerateWkt
     */
    public function providerForTestGenerateWkt()
    {
        $temp1 = array(
            0 => array(
                'LINESTRING' => array(
                    'no_of_points' => 2,
                    0 => array('x' => 5.02, 'y' => 8.45),
                    1 => array('x' => 6.14, 'y' => 0.15)
                )
            )
        );

        $temp2 = $temp1;
        $temp2[0]['LINESTRING']['no_of_points'] = 3;
        $temp2[0]['LINESTRING'][2] = array('x' => 1.56);

        $temp3 = $temp2;
        $temp3[0]['LINESTRING']['no_of_points'] = -1;

        $temp4 = $temp3;
        $temp4[0]['LINESTRING']['no_of_points'] = 3;
        unset($temp4[0]['LINESTRING'][2]['x']);

        return array(
            array(
                $temp1,
                0,
                null,
                'LINESTRING(5.02 8.45,6.14 0.15)'
            ),
            // if a coordinate is missing, default is empty string
            array(
                $temp2,
                0,
                null,
                'LINESTRING(5.02 8.45,6.14 0.15,1.56 )'
            ),
            // if no_of_points is not valid, it is considered as 2
            array(
                $temp3,
                0,
                null,
                'LINESTRING(5.02 8.45,6.14 0.15)'
            ),
            // missing coordinates are replaced with provided values (3rd parameter)
            array(
                $temp4,
                0,
                '0',
                'LINESTRING(5.02 8.45,6.14 0.15,0 0)'
            )
        );
    }

    /**
     * data provider for testGenerateParams
     *
     * @return data for testGenerateParams
     */
    public function providerForTestGenerateParams()
    {
        $temp = array(
            'LINESTRING' => array(
                'no_of_points' => 2,
                0 => array('x' => '5.02', 'y' => '8.45'),
                1 => array('x' => '6.14', 'y' => '0.15')
            )
        );
        $temp1 = $temp;
        $temp1['gis_type'] = 'LINESTRING';

        return array(
            array(
                "'LINESTRING(5.02 8.45,6.14 0.15)',124",
                null,
                array(
                    'srid' => '124',
                    0 => $temp
                )
            ),
            array(
                'LINESTRING(5.02 8.45,6.14 0.15)',
                2,
                array(
                    2 => $temp1
                )
            )
        );
    }
}
?>